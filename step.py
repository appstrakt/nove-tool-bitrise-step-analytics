import os
import json
import gspread
import base64

from datetime import datetime

try:
    # Get he credentials from the base 64
    credentials = base64.b64decode(
        os.environ['credentials'].encode('ascii')).decode('ascii')

    # Write credentials to file
    script_dir = os.path.dirname(os.path.abspath(__file__))
    google_service_account_credentials_file = "{}/google_service_account_credentials_file.json".format(
        script_dir)
    credentials_file = open(google_service_account_credentials_file, "wt")
    credentials_file.write(credentials)
    credentials_file.close()

    # Load the spreadsheet
    gc = gspread.service_account(
        filename=google_service_account_credentials_file)
    spreadsheet = gc.open_by_key(os.environ['sheet_id'])
    worksheet = spreadsheet.get_worksheet(0)

    # Create dict from all the analytics data
    tracking_data = os.environ['tracking_data']
    tracking = {x[0]: x[1] for x in [x.split("=") for x in tracking_data.strip(
        "\\n").replace("\\n", "&").split("&")]} if tracking_data else {}

    # Append data to the sheet
    start_date = datetime.fromtimestamp(int(
        os.getenv('BITRISE_BUILD_TRIGGER_TIMESTAMP')))
    end_date = datetime.now()
    total_duration = int(round((end_date-start_date).total_seconds() / 60))
    build_status = "success" if os.getenv(
        "BITRISE_BUILD_STATUS") == '0' else "failed"
    app_title = os.getenv("BITRISE_APP_TITLE")

    platform = "other"
    if "frontend-web" in app_title:
        platform = "web"
    elif "serverside-web" in app_title:
        platform = "serverside-web"
    elif "serverless" in app_title:
        platform = "serverless"
    elif "react-native" in app_title:
        platform = "react-native"
    elif "ios" in app_title:
        platform = "ios"
    elif "android" in app_title:
        platform = "android"

    # timestamp, project, build_type, status, test_status, analysis_status, duration, platform
    values = [start_date.strftime("%Y/%m/%d %H:%M:%S"), app_title, os.getenv(
        "WORKFLOW_TYPE"), build_status, tracking.get("test_status"), tracking.get("analysis_status"), total_duration, platform]

    worksheet.append_row(values, value_input_option="USER_ENTERED")

except:
    print("Error while sending analytics data")
