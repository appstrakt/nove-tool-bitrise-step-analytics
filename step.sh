#!/bin/bash
set -x
THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Function to handle errors and exit
handle_error() {
    echo "Error occurred: $1"
    exit 0  # Exiting as succeeded as per requirement
}

# Install gspread
if python3 -m pip install gspread; then
    echo "gspread installed successfully"
else
    handle_error "Failed to install gspread"
fi

# Install six
if python3 -m pip install six; then
    echo "six installed successfully"
else
    handle_error "Failed to install six"
fi

# Execute step.py
echo '$' "python "$THIS_SCRIPT_DIR/step.py""
if python3 "$THIS_SCRIPT_DIR/step.py"; then
    echo "step.py executed successfully"
else
    echo "Failed to execute step.py, but continuing..."
fi
